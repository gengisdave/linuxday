<?php

require_once ('../funzioni.php');

$year = isset($_GET['year']) ? $_GET['year'] : conf('current_year');
$events_file = '../data/events' . $year . '.json';
$new_events = [];
if (file_exists($events_file)) {
	$events = json_decode(file_get_contents($events_file));

	foreach($events as $event) {
		if ($event->approvato) {
			$new_event = new \stdClass();
			$new_event->group = $event->group;
			$new_event->city = $event->city;
			$new_event->prov = $event->prov;
			$new_event->web = $event->web;
			list($lat, $lon) = explode(',', $event->coords);
			$new_event->latitude = $lat;
			$new_event->longitude = $lon;
			$new_event->year = intval($year);
			$new_events[] = $new_event;
		}
	}
}

header("Content-Type: application/json; charset=UTF-8");
echo(json_encode($new_events));
