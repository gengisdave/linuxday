<?php

require_once ('../funzioni.php');

$csv = '';

$year = isset($_GET['year']) ? $_GET['year'] : conf('current_year');
$events_file = '../data/events' . $year . '.json';
if (file_exists($events_file)) {
	$events = json_decode(file_get_contents($events_file));

	foreach($events as $event) {
		if ($event->approvato) {
			list($lat, $lon) = explode(',', $event->coords);
			$csv .= sprintf("%s;%s;%s;%s\n", $event->group, $event->city, $event->prov, $event->web);
		}
	}
}

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=". $year .".csv");
header("Pragma: no-cache");
header("Expires: 0");
echo $csv;
