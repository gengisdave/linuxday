<?php

date_default_timezone_set("Europe/Rome");

// TODO trasformarlo in file JSON in modo da editarlo esternamente
// FIXME controllare gli ordinali delle edizioni
$edizioni = [
	'2019' => [
		'computer_date' => '2019-10-26',
		'shipping_date' => '2019-10-11',
		'human_scadenza' => '10 Ottobre 2019',
		'human_date' => 'Sabato 26 Ottobre 2019',
		'edizione' => ''
	],
	'2018' => [
		'computer_date' => '2018-10-27',
		'shipping_date' => '2018-10-15',
		'human_scadenza' => '14 Ottobre 2019',
		'human_date' => 'Sabato 27 Ottobre 2018',
		'edizione' => ''
	],
	'2017' => [
		'computer_date' => '2017-10-28',
		'shipping_date' => '2017-10-15',
		'human_scadenza' => '14 Ottobre 2019',
		'human_date' => 'Sabato 28 Ottobre 2017',
		'edizione' => ''
	],
	'2016' => [
		'computer_date' => '2016-10-22',
		'shipping_date' => '2016-10-16',
		'human_scadenza' => '15 Ottobre 2019',
		'human_date' => 'Sabato 22 Ottobre 2016',
		'edizione' => ''
	],
	'2015' => [
		'computer_date' => '2015-10-24',
		'shipping_date' => '2015-10-18',
		'human_scadenza' => '17 Ottobre 2019',
		'human_date' => 'Sabato 24 Ottobre 2015',
		'edizione' => ''
	],
	'2014' => [
		'computer_date' => '2014-10-25',
		'shipping_date' => '2014-10-19',
		'human_scadenza' => '18 Ottobre 2019',
		'human_date' => 'Sabato 25 Ottobre 2014',
		'edizione' => ''
	],
	'2013' => [
		'computer_date' => '2013-10-26',
		'shipping_date' => '2013-10-20',
		'human_scadenza' => '19 Ottobre 2019',
		'human_date' => 'Sabato 26 Ottobre 2013',
		'edizione' => ''
	]
];

$current_year = '2019';

$administrators = ['bob@linux.it', 'ferdi.traversa@gmail.com'];
