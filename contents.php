<?php

/*
LinuxDay
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function page2019howto($year) {
	pagehowto($year);
}

function page2019lineeguida($year) {
	pagelineeguida($year);
}

function page2018howto($year) {
	pagehowto($year);
}

function page2018lineeguida($year) {
	pagelineeguida($year);
}

function page2017howto($year) {
	pagehowto($year);
}

function page2017lineeguida($year) {
	pagelineeguida($year);
}

function page2016howto($year) {
	pagehowto($year);
}

function page2016lineeguida($year) {
	pagelineeguida($year);
}

function page2015howto($year) {
	pagehowto($year);
}

function page2015lineeguida($year) {
	pagelineeguida($year);
}

function page2014howto($year) {
	pagehowto($year);
}

function page2014lineeguida($year) {
	pagelineeguida($year);
}

function page2013howto($year) {
	pagehowto($year);
}

function page2013lineeguida($year) {
	pagelineeguida($year);
}

function pagehowto($year) {
	$date = Data::getDate($year);

?>

<h2>Organizzati</h2>

<p>
	Nella tua zona nessuno organizza un Linux Day? E allora, fallo tu!
</p>

<p>
	Per organizzare un Linux Day non &agrave; necessario essere un LUG, una associazione o altro: basta qualche amico, un po&apos; di buona volont&agrave;, e seguire alcuni semplici consigli e raccomandazioni.
</p>

<ul>
	<li>
		accertatevi di aver letto le <a href="/<?php echo $year . "/lineeguida"; ?>">Linee Guida</a>. Queste sono le uniche regole che occorre seguire per aderire al Linux Day: la data &agrave; quella di <?php strtolower($date['human_date']); ?>, l&apos;accesso del pubblico deve essere gratuito, e tra i temi trattati non devono apparire applicazioni proprietarie (neanche se girano su Linux!) o comunque argomenti opposti allo spirito di condivisione e apertura della community che intendete rappresentare
	</li>
	<li>
		trovate <strong>una sede</strong>. Questa &agrave; probabilmente la parte pi&ugrave; complessa dell&apos;organizzazione, in quanto per allestire qualsiasi attivit&agrave; servono almeno uno o due locali e non sempre si trovano gratuitamente o a prezzo accessibile. Sicuramente si pu&ograve; iniziare chiedendo nelle scuole e nelle sedi universitarie, o valutare qualche sala del Comune, ma sono anche apprezzate soluzioni creative come gazebo o aree coperte nelle piazze (occhio a chiedere l&apos;autorizzazione di occupazione suolo pubblico in Comune!), locali pubblici (nessuno vieta di &ldquo;occupare&rdquo; amichevolmente la birreria o il ristorante di un amico, purch&eacute; come detto sopra non ci siano obblighi di spesa per il pubblico), aree all&apos;interno dei centri commerciali (previa ovvia autorizzazione)... I limiti sono la fantasia e l&apos;audacia!
	</li>
	<li>
		definite dei <strong>contenuti</strong>. Molto dipende da quanti e quali spazi avete trovate nella fase precedente (una sala? Due? Sei?) e da quante persone sono coinvolte nel vostro gruppo. Il format pi&ugrave; comune &egraev; quello dei talk, percui sono necessari relatori che sappiano a turno illustrare i vari temi scelti (&ldquo;Cos&apos;&egrave; il Software Libero&rdquo;, &ldquo;Panoramica su Linux&rdquo;, &ldquo;Funzioni Avanzate di LibreOffice&rdquo;, o quel che volete) nonch&eacute; la disponibilit&agrave; di sedie e magari uno o pi&ugrave; proiettori per le slides. Ma non &egrave; l&apos;unica modalit&agrave;: c&apos;&egrave; chi, avendo disponibilit&agrave; di qualche computer, allestisce banchetti tematici ed interattivi presso cui &egrave; possibile circolare e soffermarsi per maggiori informazioni (e.g. progetti di elettronica, videogiochi opensource, dimostrazioni di programmi liberi per la grafica o l&apos;editing audio/video, un paio di postazioni Linux per metterci direttamente le mani...), o pi&ugrave; semplicemente si pu&ograve; allestire un punto informativo presso cui distribuire materiali, gadgets, assistenza ed informazioni (opzione particolarmente azzeccata se vi trovate in un luogo di passaggio, come appunto un gazebo in piazza o un centro commerciale)
	</li>
	<li>
		pubblicate una <string>pagina web</strong> dedicata all&apos;evento, su cui fornire i dettagli indispensabili alla partecipazione: orari, indirizzo preciso della sede, programma... Prendete ispirazione ad esempio dai siti dei Linux Day passati per capire cosa pu&ograve; essere utile comunicare, e non dimenticate di esporre il <a href="/<?php echo $year . "/immagini/linuxday_fullcolor.svg"; ?>">logo ufficiale del Linux Day</a>
	</li>
	<li>
		<strong>registrate</strong> il vostro evento su linuxday.it per essere ammessi all&apos;indice nazionale delle iniziative: questo &egrave; probabilmente il canale preferenziale per entrare in contatto con il potenziale pubblico. Per farlo, occorre <a href="/<?php echo $year . "/user"; ?>">registrare un account</a> e, dopo essersi autenticati, <a href="<?php echo $year . "/registra"; ?>">compilare questo form</a>
	</li>
	<li>
		fate <strong>promozione</strong>. La pubblicazione su linuxday.it non basta, e mai bisogna dare per scontato che le persone sappiano che ci sia il Linux Day: mandate un comunicato ai giornali locali, stampate qualche volantino da appendere nelle bacheche pubbliche, scrivete ai rappresentanti locali, insomma fatevi vedere!
	</li>
	<li>
		non fatevi cogliere alla sprovvista. Spesso capita qualcuno che porta il proprio computer per avere aiuto nell&apos;installazione di Linux, o per risolvere qualche problematica tecnica specifica: portatevi qualche CD o qualche chiavetta USB avviabile per l&apos;installazione. E preparate qualche cartello che indichi la vostra presenza, da appendere il giorno stesso intorno alla vostra zona onde rendervi più evidenti alle persone di passaggio
	</li>
	<li>
		<?php echo strtolower($date['human_date']); ?>, <strong>divertitevi</strong> il pi&ugrave; possibile!
	</li>
	<li>
		se siete soddisfatti della vostra opera, valutate la possibilit&agrave; di costituirvi in <strong>LUG</strong> permanente. Tutti i dettagli sul come e perch&eacute;, e le opportunit&agrave; di assistenza, li potete trovare nel <a href="http://www.ils.org/sites/ils.org/files/manuale_operativo_lug.pdf">&ldquo;Manuale Operativo per la Community&rdquo;</a> pubblicato da Italian Linux Society
	</li>
</ul>

<p>
	Qualcosa non &egrave; ancora chiaro? Serve maggiore supporto o chiarimenti? Non sapete dove pubblicare la pagina web per il vostro evento? Per qualsiasi informazione o richiesta potete scrivere a <a href="mailto:info@linuxday.it">info@linuxday.it</a>.
</p>

<p>
	E buon Linux Day!
</p>
<?php

}

function pagelineeguida($year) {
	$date = Data::getDate($year);

?>

<h2>Linee Guida</h2>

<p>
	Versione sintetica, per chi ha fretta.
</p>

<ul>
	<li>Il Linux Day <?php echo $year ?> si terrà <?php echo $date['human_date']; ?></li>
	<li>Il Linux Day è una manifestazione nazionale unitaria articolata in varie manifestazioni locali</li>
	<li>Essendo volto a favorire un'ampia diffusione e conoscenza di GNU/Linux e del software libero, il Linux Day si rivolge principalmente al grande pubblico</li>
	<li>Il Linux Day ha lo scopo di promuovere l'uso e la conoscenza del sistema operativo GNU/Linux e del software libero</li>
	<li>L'accesso alla manifestazione deve sempre essere libero e gratuito</li>
	<li>Agli organizzatori locali è lasciata la possibilità di eventuali raccolte fondi</li>
</ul>

<p>
	Versione estesa, per chi vuole saperne di più.
</p>

<ol>
	<li>
		il Linux Day ha lo scopo di promuovere l'uso e la conoscenza del sistema operativo GNU/Linux e del software libero. è eventualmente possibile prendere in considerazione anche altri sistemi operativi liberi, purché la manifestazione sia comunque incentrata su GNU/Linux. Il fine è la promozione di GNU/Linux in quanto software libero, e non la promozione di qualunque programma che giri su GNU/Linux; è comunque possibile accennare marginalmente a software proprietario relativo a GNU/Linux per completezza di informazione, ad esempio per questioni di interoperabilità, o in mancanza di un equivalente libero eccetera, purché tale accenno sia motivato rispetto allo scopo della manifestazione e non vi sia assolutamente l'intento di promuovere tale software. è inoltre possibile accennare alla compatibilità con sistemi proprietari relativamente a software libero disponibile, oltre che per GNU/Linux, anche per tali sistemi. In ogni caso la responsabilità di ciò che viene mostrato, se non fa parte dei materiali comuni, ricade esclusivamente sul singolo gruppo locale (ad esempio per questioni di licenze) e non su ILS o sui restanti organizzatori
	</li>
	<li>
		Essendo volto a favorire un'ampia diffusione e conoscenza di GNU/Linux e del software libero, il Linux Day si rivolge principalmente al grande pubblico. Questo non esclude la possibilità di trattare argomenti avanzati e specialistici, tuttavia è necessario non trascurare attività e interventi destinati agli utenti meno esperti, sia per quanto riguarda gli aspetti tecnici che il concetto stesso di software libero.
	</li>
	<li>
		L'accesso alla manifestazione deve sempre essere libero e gratuito , in ogni sua parte e momento. Non sono ammesse forme di ingresso a pagamento o dietro iscrizioni obbligatorie, qualunque sia il gruppo o associazione interessato.
	</li>
	<li>
		L'organizzazione della manifestazione non deve essere in alcun modo legata ad attività di gruppi politici, religiosi o di qualunque altro tipo che non perseguano le finalità della manifestazione stessa. Non è quindi accettabile che tali soggetti facciano parte dell'organizzazione, che ospitino la manifestazione (o parte di essa) all'interno di proprie iniziative o collegandola con proprie iniziative e attività, o che intervengano durante la stessa se non per perseguire gli obiettivi della manifestazione come esposti al punto 1. Questo non esclude eventuali rapporti con figure istituzionali.
	</li>
	<li>
		Agli organizzatori locali è lasciata la possibilità di eventuali raccolte fondi, ad esempio con offerte volontarie, vendita di gadget o raccolta di nuove iscrizioni, da gestire in proprio. Qualunque responsabilità in tal senso, ad esempio per questioni fiscali, ricade sul relativo gruppo locale e non sui restanti organizzatori. Eventuali gadget o materiali forniti gratuitamente a livello nazionale tramite ILS per la distribuzione durante la manifestazione devono comunque essere distribuiti gratuitamente: non possono quindi essere venduti, nè è possibile distribuirli in cambio di un'offerta o dell'iscrizione a un'associazione.
	</li>
	<li>
		È ammessa la possibilità di sponsorizzazioni della manifestazione, sia a livello nazionale che per i singoli eventi locali, con le medesime regole. L'eventuale presenza degli sponsor, sia nazionali che locali, deve comunque essere in linea con gli obiettivi della manifestazione come esposti al punto 1 e deve essere salvaguardato il carattere non commerciale, plurale e indipendente della manifestazione stessa. Al fine di evitare che un evento locale sia focalizzato sulla presentazione di una particolare azienda e/o dei suoi prodotti e servizi, il che non è ammissibile, dovranno essere rispettati i seguenti punti
		<ul>
			<li>sono ammesse donazioni in denaro, da qualsiasi ente o azienda, all'organizzatore;</li>
			<li>sono ammesse donazioni in materiale attinente al software libero (cdrom, libri, documentazione, gadget, magliette, penne, cartelline...);</li>
			<li>non è ammessa la distribuzione di materiale che non riguardi il software libero;</li>
			<li>uno sponsor non può cedere ad altri enti, aziende o marchi i diritti derivanti dalla sponsorizzazione dell'evento senza prima aver interpellato gli organizzatori (locali o nazionali) di riferimento.</li>
		</ul>
	</li>
	<li>
		Il Linux Day è una manifestazione nazionale unitaria articolata in varie manifestazioni locali. è necessario che l'aspetto unitario sia evidente e venga sottolineato, utilizzando sempre e solo la denominazione ufficiale "Linux Day <?php echo $year ?> - <?php echo $date['edizione']; ?> giornata nazionale di Linux e del software libero" (brevemente "Linux Day <?php echo $year ?>", "Linux Day", o in sigla "LD<?php echo substr($year, 2, 2); ?>"), inserendo nel sito web e sul materiale dedicato alla manifestazione locale il riferimento al sito web nazionale, e utilizzando logo, manifesto, volantini e altro materiale comune che potrà essere messo a disposizione.
	</li>
	<li>
		Il Linux Day <?php echo $year ?> si terrà <?php echo strtolower($date['human_date']); ?>. Ciascun gruppo organizzatore ha facoltà di organizzare anche eventi collaterali legati al Linux Day nei giorni dal giovedì precedente alla domenica successiva, purché l'evento principale si tenga <?php echo strtolower($date['human_date']); ?>. L'evento principale e quelli collaterali ad esso legati possono anche svolgersi in località vicine; è comunque necessario che il gruppo organizzatore sia unificato e che gli eventi vengano gestiti e pubblicizzati in maniera unitaria e secondo un programma unico. Anche gli eventi collaterali devono essere conformi alle linee guida qui specificate.
	</li>
	<li>
		Affinché il proprio evento locale sia incluso nella manifestazione è necessario darne comunicazione sufficientemente dettagliata, includendo il proprio programma di massima, a ILS entro e non oltre il <?php echo $date['human_scadenza']; ?>. ILS si riserva la facoltà di non includere eventi che, a proprio giudizio, appaiano non conformi con lo spirito della manifestazione e con le presenti linee guida, nonché eventi organizzati da gruppi che, in precedenti edizioni, abbiano organizzato eventi non conformi con lo spirito della manifestazione.
	</li>
</ol>
<?php

}
?>
