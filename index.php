<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('funzioni.php');

$year = (isset($_GET['anno'])) ? $_GET['anno'] : conf('current_year');
$page = (isset($_GET['page'])) ? $_GET['page'] : 'main';
$date = Data::getDate($year);

if (file_exists($events_file)) {
	$events = json_decode(file_get_contents($events_file));

	$events = array_filter($events, function($a) {
		return $a->approvato;
	});

	usort($events, function($a, $b) {
		if ($a->prov < $b->prov)
			return -1;
		else if ($b->prov < $a->prov)
			return 1;
		else
			return 0;
	});
}
else {
	$events = [];
}

	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="italian" />
	<meta name="robots" content="noarchive" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://www.linux.it/shared/index.php?f=bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="https://www.linux.it/shared/index.php?f=main.css" rel="stylesheet" type="text/css" />

	<meta name="dcterms.creator" content="Italian Linux Society" />
	<meta name="dcterms.type" content="Text" />
	<link rel="publisher" href="http://www.ils.org/" />

	<meta name="twitter:title" content="Linux Day, <?php echo $date['human_date']; ?>" />
	<meta name="twitter:creator" content="@LinuxDayItalia" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="<?php echo makeurl('/') ?>" />
	<meta name="twitter:image" content="<?php echo makeurl('/immagini/tw.png') ?>" />

	<meta property="og:site_name" content="Linux Day" />
	<meta property="og:title" content="Linux Day, <?php echo $date['human_date']; ?>" />
	<meta property="og:url" content="<?php echo makeurl('/') ?>" />
	<meta property="og:image" content="<?php echo makeurl('/immagini/fb.png') ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:country-name" content="Italy" />
	<meta property="og:email" content="webmaster@linux.it" />
	<meta property="og:locale" content="it_IT" />
	<meta property="og:description" content="Giornata Nazionale per il Software Libero" />

	<script type="text/javascript" src="<?php echo makeurl('/js/jquery.slim.min.js') ?>"></script>

	<link href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" rel="stylesheet" type="text/css" />
	<link href="/registra/registra.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>

	<title><?php echo Page::setTitle($year, $page); ?></title>
</head>
<body>

<!-- Header -->
<div id="header">
	<img src="<?php echo makeurl('/immagini/logo.png') ?>" width="79" height="79" alt="Linux Day" />
	<div id="maintitle">Linux Day <span class="hidden-sm"><?php echo $year ?></span></div>
	<div id="payoff">Giornata Nazionale per il Software Libero</div>

	<div class="menu">
		<a class="generalink" href="/<?php echo $year; ?>/">Home</a>
		<a class="generalink" href="/<?php echo $year; ?>/howto/">Organizzati</a>
		<a class="generalink" href="/<?php echo $year; ?>/promozione/">Promozione</a>

		<?php if(!empty($_SESSION['user_email'])): ?>
			<a class="generalink" href="<?php echo makeurl('/registra/index.php') ?>">Il mio LinuxDay</a>
			<a class="generalink" href="<?php echo makeurl('/user/?action=logout') ?>">Logout</a>
		<?php else: ?>
			<a class="generalink" href="<?php echo makeurl('/user') ?>">Login</a>
		<?php endif ?>

		<?php if(isset($_SESSION['admin'])): ?>
			<a class="generalink" href="<?php echo makeurl('/admin/index.php') ?>">Admin</a>
		<?php endif ?>

		<p class="social mt-2">
			<a href="https://twitter.com/LinuxDayItalia"><img src="https://www.linux.it/shared/index.php?f=immagini/twitter.png"></a>
			<a href="https://www.facebook.com/LinuxDayItalia"><img src="https://www.linux.it/shared/index.php?f=immagini/facebook.png"></a>
		</p>
	</div>
</div>
<!-- End Header Code -->

<div class="container mt-5">
	<div class="row">
		<div class="col-md-3 text-center promoters">
			<h5>Promosso da</h5>
			<a href="https://www.ils.org/">
				<img class="img-fluid" src="<?php echo makeurl('/immagini/ils.png') ?>" alt="Italian Linux Society">
			</a>
			<br><br><br>
			<h5>Col Sostegno di</h5>
			<a href="https://www.lpi.org/it/">
				<img class="img-fluid" src="<?php echo makeurl('/immagini/lpi.png') ?>" alt="Linux Professional Institute Italia">
			</a>
		</div>

		<div class="col-md-9 main-contents pl-5">

<?php

switch ($page) {

	case 'main':
	case 'howto':
	case 'promozione':
	case 'lineeguida':
		echo Page::drawPage($year, $page);
		break;
	default:
		echo Page::drawPage($year, 'main');
		break;

}

?>

<!-- from here -->
<?php if(count($events) != 0): ?>
	<div id="mapid"></div>

	<script>
	$(document).ready(function() {
		var mymap = L.map('mapid').setView([42.204, 11.711], 6);

		L.tileLayer('https://c.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
			maxZoom: 18,
		}).addTo(mymap);

		<?php
		foreach($events as $event) {
			?>
			L.marker([<?php echo($event->coords) ?>]).bindPopup('<b><?php echo($event->group) ?></b> </br> <?php echo($event->city) ?> (<?php echo($event->prov) ?>) </br> <a href="<?php echo($event->web)?>"><?php echo($event->web)?></a>').addTo(mymap);
			<?php
		}
		?>
	});
	</script>

	<table class="table">
		<thead>
			<tr>
				<th width="50%">Organizzatore</th>
				<th width="45%">Città</th>
				<th width="5%">Link</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($events as $event) {
				?>
				<tr>
					<td><?php echo $event->group ?></td>
					<td><?php echo $event->city ?> (<?php echo $event->prov ?>)</td>
					<td><a href="<?php echo $event->web ?>">Link</a></td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
<?php endif ?>
<!-- to here -->

		</div>
	</div>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<!-- Footer -->
<div id="ils_footer" class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<span style="text-align: center; display: block">
					<a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/agpl3.svg" style="border-width:0" alt="AGPLv3 License">
					</a>

					<a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/cczero.png" style="border-width:0" alt="Creative Commons License">
					</a>
				</span>
			</div>

			<div class="col-md-3">
				<h2>RESTA AGGIORNATO!</h2>
				<p>
					Iscriviti alla newsletter per aggiornamenti periodici sul software libero in Italia!
				</p>

				<p>
					Specificando la tua provincia di residenza riceverai anche gli annunci sulle <a href="https://www.linux.it/eventi">attività svolte dai LUG e dai gruppi amici</a> nella tua zona.
				</p>

				<form class="webform-client-form" action="https://www.linux.it/subscribe.php" method="get">
					<input type="email" class="form-control" name="name" placeholder="Indirizzo Mail" />
					<p style="display: none">
						<input type="text" name="mail" />
					</p>

					<?php prov_select ('form-control'); ?>

					<input type="submit" class="form-control" value="Iscriviti" />
				</form>
			</div>

			<div class="col-md-3">
				<h2>Amici</h2>
				<p style="text-align: center">
					<a href="http://www.ils.org/info#aderenti">
						<img src="https://www.ils.org/sites/ils.org/files/associazioni/getrand.php" border="0" /><br />
						Scopri tutte le associazioni che hanno aderito a ILS.
					</a>
				</p>
			</div>

			<div class="col-md-3">
				<h2>Network</h2>
				<script type="text/javaScript" src="https://www.linux.it/external/widgetils.php?referrer=linuxday"></script>
				<div id="widgetils"></div>
			</div>
		</div>
	</div>

	<div style="clear: both"></div>
</div>
<!-- End Footer Code -->

<!-- Matomo -->
<script type="text/javascript">
	var _paq = window._paq || [];
	/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
	_paq.push(["setCookieDomain", "*.www.linuxday.it"]);
	_paq.push(["setDomains", ["*.www.linuxday.it","*.www.linuxday.it"]]);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u="//stats.madbob.org/";
		_paq.push(['setTrackerUrl', u+'matomo.php']);
		_paq.push(['setSiteId', '13']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Matomo Code -->

</body>
</html>
